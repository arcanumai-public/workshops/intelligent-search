import React, {useState} from 'react';
import FeedbackButton from './feedbackButton';

const SearchComponent = () => {

    const [searchSubmit, setSearchSubmit] = useState(false);
    const [searchField, setSearchField] = useState('');
    const searchData = {
        "query": searchField
    }
    const [searchResults, setSearchResults] = useState([]);
    const [searchQueryId, setSearchQueryId] = useState('');

    const handleChange = (event) => {
        setSearchField(event.target.value);
    }

    const handleSubmit = () => {
        setSearchSubmit(true);

        fetch(
            'http://nlp-document-store-dev-130100662.ap-southeast-2.elb.amazonaws.com/api/nlp-document-store/query',
            {
                method: 'POST',
                headers: {
                    'Content-type': 'application/json'
                },
                body: JSON.stringify(searchData)
            }
        ).then(
            (Response) => Response.json()
        ).then(
            (Response) => {
                console.log(Response);
                console.log(Response.results);
                setSearchResults(Response.results);
                setSearchQueryId(Response.queryId);
            }
        )
    }

    if (searchSubmit) {
        return(
            <>
                <h2>Search Result</h2>
                <ul>
                    {searchResults.map(
                        searchResult => {
                            return(
                                <li key={searchResult.id}>
                                    <h4>{searchResult.document}</h4>
                                    <p>{searchResult.documentText}</p>
                                    <a href={searchResult.documentUri + '#page=' + searchResult.excerptPage} target="_blank" rel="noreferrer">Download Document</a>
                                    <FeedbackButton qid={searchQueryId} rid={searchResult.id} />
                                </li>
                            )
                        }
                    )}
                </ul>
            </>
        )
    }

    return(
        <>
            <h2>Search Document</h2>
            <form onSubmit={handleSubmit}>
                <input type="text" value={searchField} onChange={handleChange} />
                <button type="submit">Search</button>
            </form>
        </>
    )

}

export default SearchComponent;
