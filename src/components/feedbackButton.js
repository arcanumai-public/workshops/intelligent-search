import React, { useState } from "react";

const FeedbackButton = (props) => {

    const [feedbacked, setFeedbacked] = useState(false);

    const handleClick = value => () => {

        /* Instructions:

        1. Create a variable for this json and assign it to fetch( body: )

        {
            "queryId": props.qid,
            "reviews": [
              {
                "relevancevalue": value,
                "resultid": props.rid
              }
            ]
        }

        2. Use fetch to make a post request on 'http://nlp-document-store-dev-130100662.ap-southeast-2.elb.amazonaws.com/api/nlp-document-store/feedback'
        
        fetch()
        
        3. Assign the setFeedbacked(true) state on the API response

        You can refer to search.js component codes as your guide

        Goodluck!

        */
        
        const feedbackData = {
            "queryId": props.qid,
            "reviews": [
              {
                "relevancevalue": value,
                "resultid": props.rid
              }
            ]
        }

        fetch(
            'http://nlp-document-store-dev-130100662.ap-southeast-2.elb.amazonaws.com/api/nlp-document-store/feedback',
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(feedbackData)
            }
        )
        .then(
            (Response) => Response.json()
        )
        .then(
            (feedbackResponse) => {
                console.log(feedbackResponse);
                setFeedbacked(true);
            }
        )

    }

    if(feedbacked) {
        return(
            <div style={{marginTop: 10}}>
                Ranked
            </div>
        )
    }

    return(
        <div style={{marginTop: 10}}>
            <button onClick={handleClick('RELEVANT')}>Rate this result</button>
        </div>
    )

}

export default FeedbackButton;