# Machine Learning Workshop 

Frontend & ML Service Integration

## Available Scripts

In the project directory, you can run:

### `npm install`

To install the current project's dependencies.

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

## Intelligent Search Documentation

http://nlp-document-store-dev-130100662.ap-southeast-2.elb.amazonaws.com/api/nlp-document-store/docs/

## Learn ReactJS and JSX

Learn React https://reactjs.org/ and JSX https://reactjs.org/docs/introducing-jsx.html

## Get in touch

Eddril Lacorte\
Frontend Developer | Arcanum AI

Email: eddril@arcanum.ai\
LinkedIn: https://www.linkedin.com/in/eddrillacorte/